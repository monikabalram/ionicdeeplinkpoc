import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Deeplinks } from '../../node_modules/@ionic-native/deeplinks';
import { PostsPage } from '../pages/posts/posts';
import { PostDetailsPage } from '../pages/post-details/post-details';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = PostsPage;
  @ViewChild(Nav) navChild:Nav;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private deeplink: Deeplinks) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.deeplink.route({
        '/posts/:postId' : PostDetailsPage
      }).subscribe ((match) => {
        alert(JSON.stringify(match));
        console.log("Deeplink match :::",JSON.stringify(match.$args.postId));
        var id: number = match.$args.postId;
        if(id) {
          this.navChild.setRoot(PostDetailsPage,{'postId':id});
        }
      }, (nomatch) => {
        console.log("deeplink nomatch message :::", nomatch);
      })
    });
  }
}
