import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { Post } from '../../models/posts.interface';
import { Observable } from '../../../node_modules/rxjs/Observable';

/**
 * Generated class for the PostDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage(
//   {
//   segment: 'posts/:postId',
//   defaultHistory: ['PostsPage']
// }
)
@Component({
  selector: 'page-post-details',
  templateUrl: 'post-details.html',
})
export class PostDetailsPage {

  postId: number;
  post : Observable<Post>;
  constructor(private data: DataProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.postId = this.navParams.get('postId');
    console.log('post ID : ',this.postId);
    this.post = this.data.getPostById(this.postId);
  }

}
